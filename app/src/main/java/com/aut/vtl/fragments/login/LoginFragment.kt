package com.aut.vtl.fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aut.vtl.R
import com.aut.vtl.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var binding : FragmentLoginBinding
    /** View Model */
    private val viewModel: LoginViewModel by lazy {
        ViewModelProvider(this).get(LoginViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.login)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_login, container, false
        )

        /** On click listener for Login Button*/
        binding.loginButton.setOnClickListener {
            viewModel.setAuthenticationStatus(binding.emailInput.text.toString(),
                binding.passwordInput.text.toString()
            )
        }


        /** On click listener for Dont have an account text*/
        binding.noAccountText.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signUpFragment)
        }

        /**
         * 0 means default
         * 1 means no inputs are filled
         * 2 means email not filled
         * 3 means email format is wrong
         * 4 means password not filled
         * 5 means user does not exit
         * 6 means incorrect password
         * 99 means inputs are successfully validated and call the login API
         * 100 means API response is 200(successfully authenticated and navigate to Traffic light screen)
         * */
        val authStatusObserver  = Observer<Int> { status ->
            if (status != 0) {
                binding.loginErrorText.visibility = View.VISIBLE
            }
            if ( status == 99) {
                binding.loginErrorText.setTextColor(
                    ContextCompat.getColor(requireActivity(), R.color.colorBlack)
                )
            } else {
                binding.loginErrorText.setTextColor(
                    ContextCompat.getColor(requireActivity(), R.color.colorRed)
                )
            }
            when (status) {
                1 -> {
                    binding.loginErrorText.setText(R.string.fill_email_and_password_error)
                }
                2 -> {
                    binding.loginErrorText.setText(R.string.fill_email_error)
                }
                3 -> {
                    binding.loginErrorText.setText(R.string.invalid_email)
                }
                4 -> {
                    binding.loginErrorText.setText(R.string.fill_password_error)
                }
                5 -> {
                    binding.loginErrorText.setText(R.string.no_user)
                }
                6 -> {
                    binding.loginErrorText.setText(R.string.incorrect_password)
                }
                99 -> {
                    binding.loginErrorText.setText(R.string.signing)
                    viewModel.authenticate(binding.emailInput.text.toString(),
                        binding.passwordInput.text.toString())
                }
                100 -> {
                    findNavController().navigate(R.id.action_loginFragment_to_trafficLightFragment)
                    viewModel.resetAuthStatus()
                }
            }
        }


        viewModel.authenticationStatus.observe(viewLifecycleOwner, authStatusObserver)

        return binding.root
    }




};