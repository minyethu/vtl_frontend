package com.aut.vtl.fragments.trafficLight

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TrafficLightViewModel : ViewModel() {

    private val _trafficCondition = MutableLiveData<String>()

    val trafficCondition: LiveData<String>
        get() = _trafficCondition

    init {
        _trafficCondition.value = "GREEN"
    }

    fun startTimer(duration: Long) {
        val timer = object: CountDownTimer(duration, 1000) {
            override fun onFinish() {
                changeTrafficCondition()
            }

            override fun onTick(millisUntilFinished: Long) {
                Log.i("Count", "Counting")
            }
        }
        timer.start()
    }

    private fun changeTrafficCondition() {
        when (_trafficCondition.value ) {
            "GREEN" -> {
                _trafficCondition.value  = "YELLOW"
            }
            "YELLOW" -> {
                _trafficCondition.value  = "RED"
            }
            else -> {
                _trafficCondition.value  = "GREEN"
            }
        }
    }
}