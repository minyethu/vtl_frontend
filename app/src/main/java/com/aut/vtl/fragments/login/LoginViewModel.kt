package com.aut.vtl.fragments.login

import UserLogIn
import VtlApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aut.vtl.util.UtilClass
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.HttpException


class LoginViewModel: ViewModel() {

    private val _authenticationStatus = MutableLiveData<Int>()
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main +  viewModelJob)

    val authenticationStatus: LiveData<Int>
        get() = _authenticationStatus

    init {
        _authenticationStatus.value = 0
    }

    fun setAuthenticationStatus (email: String, password: String) {
        _authenticationStatus.value = validateInputs(email, password)
    }

    private fun validateInputs(email: String, password: String): Int {
        if (email == "" && password == "") {
           return 1
        } else if (email == "") {
            return 2
        }else if (!UtilClass.validateEmail(email)) {
            return 3
        } else if (password == "") {
            return 4
        }
        return 99
    }

    fun authenticate (email: String, password: String) {
        val user = UserLogIn(email, password)
        coroutineScope.launch {
            val getLoginDeferred = VtlApi.retrofitService.userAuthAsync(user)
            try {
                getLoginDeferred.await();
                _authenticationStatus.value = 100
            } catch (e: HttpException) {
                val errorObject = JSONObject(e.response().errorBody()?.string())
                val errorCode = errorObject.getString("errorCode")
                if (errorCode == "NO_USER") {
                    _authenticationStatus.value = 5
                } else if (errorCode == "INCORRECT_PASSWORD") {
                    _authenticationStatus.value = 6
                }
            }
        }
    }

    fun resetAuthStatus() {
        _authenticationStatus.value = 0
    }
}