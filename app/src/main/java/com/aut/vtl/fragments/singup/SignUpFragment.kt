package com.aut.vtl.fragments.singup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aut.vtl.R
import com.aut.vtl.databinding.FragmentSignupBinding

class SignUpFragment : Fragment() {

    private lateinit var binding : FragmentSignupBinding
    /** View Model */
    private val viewModel: SignUpViewModel by lazy {
        ViewModelProvider(this).get(SignUpViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.sign_up)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_signup, container, false
        )

        /** On click listener for Login Button*/
        binding.signUpButton.setOnClickListener {
            viewModel.setErrorStatus(
                    binding.nameInput.text.toString(),
            binding.emailSignUpInput.text.toString(),
            binding.passwordInput.text.toString(),
            binding.passwordConfirmInput.text.toString()
            )
        }

        /**
         * 0 means default
         * 1 means no inputs are filled
         * 3 email format is wrong
         * 6 means password and confirm password do not match
         * 7 means user with the email already exists
         * 99 means inputs are successfully validated and call the SignUp API
         * 100 means API response is 200(successfully created user and navigate to Traffic light screen)
         * */
        val errorStatusObserver  = Observer<Int> { status ->
            if (status != 0) {
                binding.signUpInputErrorText.visibility = View.VISIBLE
            }
            if ( status == 99) {
                binding.signUpInputErrorText.setTextColor(
                    ContextCompat.getColor(requireActivity(), R.color.colorBlack)
                )
            } else {
                binding.signUpInputErrorText.setTextColor(
                    ContextCompat.getColor(requireActivity(), R.color.colorRed)
                )
            }
            when (status) {
                1 -> {
                    binding.signUpInputErrorText.setText(R.string.fill_all_inputs)
                }
                3 -> {
                    binding.signUpInputErrorText.setText(R.string.invalid_email)
                }
                6 -> {
                    binding.signUpInputErrorText.setText(R.string.password_do_not_match)
                }
                7 -> {
                    binding.signUpInputErrorText.setText(R.string.user_exist_error)
                }
                99 -> {
                    binding.signUpInputErrorText.setText(R.string.creating)
                    viewModel.signUp(
                        binding.nameInput.text.toString(),
                        binding.emailSignUpInput.text.toString(),
                        binding.passwordInput.text.toString())
                }
                100 -> {
                    findNavController().popBackStack()
                    Toast.makeText(this.context, getString(R.string.user_account_created), Toast.LENGTH_SHORT).show()
                    viewModel.resetErrorStatus()
                }
            }
        }


        viewModel.errorStatus.observe(viewLifecycleOwner, errorStatusObserver)

        return binding.root
    }

};