
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aut.vtl.R
import com.aut.vtl.databinding.FragmentTrafficLightBinding
import com.aut.vtl.fragments.trafficLight.TrafficLightViewModel

class TrafficLightFragment : Fragment() {

    private lateinit var binding : FragmentTrafficLightBinding
    /** View Model */
    private val viewModel: TrafficLightViewModel by lazy {
        ViewModelProvider(this).get(TrafficLightViewModel::class.java)
    }

    /** When the user is at Traffic Light Screen, pressing back button will close the app instead
     * of going back to login screen
     * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finish();
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.traffic)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_traffic_light, container, false
        )

        val trafficObserver  = Observer<String> { condition ->
            when (condition) {
                "GREEN" -> {
                    setTraffic(
                        R.string.green_traffic, R.drawable.green_circle,
                        R.color.colorGreen, 20000
                    )
                }
                "YELLOW" -> {
                    setTraffic(
                        R.string.yellow_traffic, R.drawable.yellow_circle,
                        R.color.colorYellow, 5000
                    )
                }
                else -> {
                    setTraffic(
                        R.string.red_traffic, R.drawable.red_circle,
                        R.color.colorRed, 20000
                    )
                }
            }
        }

        viewModel.trafficCondition.observe(viewLifecycleOwner, trafficObserver)

        return binding.root
    }

    private fun setTraffic(displayText: Int, image: Int, color: Int, duration: Long) {
        binding.trafficInfoText.setText(displayText)
        binding.trafficLightImage.setImageResource(image)
        binding.trafficInfoText.setTextColor(
            ContextCompat.getColor(requireActivity(), color)
        )
        viewModel.startTimer(duration)
    }

};