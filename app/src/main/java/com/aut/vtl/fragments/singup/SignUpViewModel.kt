package com.aut.vtl.fragments.singup

import UserSignUp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aut.vtl.util.UtilClass
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.HttpException

class SignUpViewModel: ViewModel() {
    private val _errorStatus = MutableLiveData<Int>()
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main +  viewModelJob)

    val errorStatus: LiveData<Int>
        get() = _errorStatus

    init {
        _errorStatus.value = 0
    }

    fun setErrorStatus (username: String, email: String, password: String,
        confirmPassword: String) {
        _errorStatus.value = validateInputs(username, email, password, confirmPassword)
    }

    private fun validateInputs(username: String, email: String, password: String,
        confirmPassword: String): Int {
        if (username == "" || email == "" ||  password == "" || confirmPassword == "") {
            return 1
        } else if (!UtilClass.validateEmail(email)) {
            return 3
        } else if (password != confirmPassword) {
            return 6
        }
        return 99
    }

    fun signUp (username: String, email: String, password: String) {
        val user = UserSignUp(username, email, password)
        coroutineScope.launch {
            val getLoginDeferred = VtlApi.retrofitService.userSignUpAsync(user)
            try {
                getLoginDeferred.await();
                _errorStatus.value = 100
            } catch (e: HttpException) {
                val errorObject = JSONObject(e.response().errorBody()?.string())
                val errorCode = errorObject.getString("errorCode")
                if (errorCode == "USER_EXIST") {
                    _errorStatus.value = 7
                }
            }
        }
    }

    fun resetErrorStatus () {
        _errorStatus.value = 0
    }
}