package com.aut.vtl.util

object UtilClass {
    @JvmStatic
    fun validateEmail(email: String): Boolean {
        val EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$".toRegex()
        return(email.matches(EMAIL_REGEX))
    }
}