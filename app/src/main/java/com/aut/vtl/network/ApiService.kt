import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST


private const val BASE_URL = "http://192.168.1.77:3000"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()


data class UserSignUp(
    val username: String,
    val email: String,
    val password: String
)

data class UserLogIn(
    val email: String,
    val password: String
)

data class User(
    val token: String
)

interface ApiService {
    @POST("/user/signup")
    fun userSignUpAsync(@Body body: UserSignUp):
            Deferred<User>

    @POST("/user/login")
    fun userAuthAsync(@Body body: UserLogIn):
            Deferred<User>

}

object VtlApi {
    val retrofitService : ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}