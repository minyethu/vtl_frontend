package com.aut.vtl

import com.aut.vtl.fragments.login.LoginViewModel
import com.aut.vtl.util.UtilClass
import org.junit.Test

import org.junit.Assert.*


class LoginViewModelFuncTest {
    @Test
    fun test_invalid_email1() {
        assertEquals( false, UtilClass.validateEmail("test@gmail."))
    }

    @Test
    fun test_invalid_email2() {
        assertEquals(false, UtilClass.validateEmail("tes@t@gmail.com"))
    }

    @Test
    fun test_invalid_email3() {
        assertEquals(false, UtilClass.validateEmail("testgmail.com"))
    }

    @Test
    fun test_invalid_email4() {
        assertEquals(false, UtilClass.validateEmail("@.com"))
    }

    @Test
    fun test_valid_email() {
        assertEquals(true, UtilClass.validateEmail("test@gmail.com"))
    }

    @Test
    fun test_valid_email2() {
        assertEquals(true, UtilClass.validateEmail("test@gmail.com.nz"))
    }

    @Test
    fun test_valid_email3() {
        assertEquals(true, UtilClass.validateEmail("test.tester@gmail.com"))
    }


}